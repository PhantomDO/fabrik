# FABRIK

Nous avons étudié la cinématique inverse via l'implémentation de l'algorithme FABRIK. Nous avons mis en pratique l'algorithme dans Unity, afin d'avoir un visuel et de quoi pouvoir manipuler les bones dans un éditeur. L'algorithme est une version simplifiée et rapide de la cinématique inverse, il fonctionne en deux phase :  

- Forward : part du point le plus en profondeur et remonte jusqu'au root

- Backward : part du root jusqu'au point le plus en profondeur 

On utilise ces deux phases pour modifier les positions de chaque bones avec des contraintes sur les jointures. 

Toujours en simulation physique, nous avons implémenter une pseudo simulation de particules (fluide viscoelastique), de mon côté étant donné que l'algorithme n'est pas très rapide j'ai décidé d'utiliser les compute shader pour accélérer les calculs de mouvement des particules. Je n'ai pas eu le temps d'implémenter la structures accélératrice utiliser en synthèse d'image sur GPU cependant (scalable gpu fluid simulation). 
