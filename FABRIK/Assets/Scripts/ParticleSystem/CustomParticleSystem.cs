using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

[System.Serializable]
public struct Particle
{
    public Vector3 position;
    public Vector3 velocity;
    public Vector4 color;

    public static int Size()
    {
        return sizeof(float) * 3 * 2 + sizeof(float) * 4;
    }
}

public class CustomParticleSystem : MonoBehaviour
{
    public bool GPUDRAW = false;

    public int EntityCount;
    public GameObject EntityPrefab;
    public Transform EntityStart;
    public GameObject[] Entities;
    
    public float fireRate = 0.02f;
    public float moveSpeed = 50f;

    public BoxCollider Collider;

    public List<int> Kernels;
    
    [Tooltip("Has to be a multiple of 2 or 3 for better integration")]
    public uint GridCubeRadius = 4;


    private void OnDrawGizmos()
    {
        if (Collider)
        {
            var bounds = Collider.bounds;
            var size = bounds.size;
            var center = bounds.center;
            var extends = bounds.extents;

            var width = size / GridCubeRadius;

            for (int z = 0; z < GridCubeRadius; z++)
            {
                for (int y = 0; y < GridCubeRadius; y++)
                {
                    for (int x = 0; x < GridCubeRadius; x++)
                    {
                        var pos = center;
                        pos += new Vector3(-extends.x + x * width.x, extends.y - y * width.y, extends.z - z * width.z);
                        pos += new Vector3(width.x / 2, -width.y / 2, -width.z / 2);
                        
                        Gizmos.DrawWireCube(pos, width);
                    }
                }
            }
        }
    }


    #region Shader

    public float range = 10.0f;
    public Color color;
    public Material material;
    public ComputeShader compute;

    [Header("Shader Params")]
    public Vector3 _Gravity;
    public float _DensityZero;
    public float _Stiffness;
    public float _StiffnessNear;
    public float _InteractionRadius;

    [SerializeField] private Particle[] _entityProperties;

    private Mesh mesh;
    private Bounds bounds;
    private ComputeBuffer entityOldPositionBuffer;
    private ComputeBuffer entityPropertiesBuffer;
    private ComputeBuffer argsBuffer;
    private static readonly int Properties = Shader.PropertyToID("_Properties");
    private static readonly int OldPositions = Shader.PropertyToID("_OldPositions");

    private void Setup()
    {
        Mesh mesh = CreateQuad();
        this.mesh = mesh;

        bounds = new Bounds(transform.position, Collider.bounds.size);
        InitBuffers();
    }

    private void FindKernels()
    {
        Kernels = new List<int>
        {
            compute.FindKernel("ApplyGravity"),
            compute.FindKernel("UpdatePosition"),
            compute.FindKernel("DoubleDensityRelaxation"),
            compute.FindKernel("UpdateVelocity")
        };
    }

    private void InitBuffers()
    {
        FindKernels();

        uint[] args = new uint[5] {0, 0, 0, 0, 0};
        args[0] = (uint) mesh.GetIndexCount(0);
        args[1] = (uint) EntityCount;
        args[2] = (uint) mesh.GetIndexStart(0);
        args[3] = (uint) mesh.GetBaseVertex(0);
        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
        argsBuffer.SetData(args);
        
        Vector3[] oldPositions = new Vector3[EntityCount];
        Particle[] properties = new Particle[EntityCount];
        for (int i = 0; i < EntityCount; i++)
        {
            var prop = new Particle();
            var position = new Vector3(Random.Range(-range, range), Random.Range(-range, range),
                Random.Range(-range, range));
            var velocity = Vector3.zero;

            prop.position = position;
            prop.velocity = velocity;
            prop.color = color;
            properties[i] = prop;
            oldPositions[i] = position;
        }

        entityPropertiesBuffer = new ComputeBuffer(EntityCount, Particle.Size());
        entityPropertiesBuffer.SetData(properties);

        entityOldPositionBuffer = new ComputeBuffer(EntityCount, sizeof(float) * 3);
        entityOldPositionBuffer.SetData(oldPositions);
        
        compute.SetInt("_EntityCount", EntityCount);
        compute.SetVector("_Gravity", _Gravity);
        compute.SetVector("_BoundsMin", bounds.min);
        compute.SetVector("_BoundsMax", bounds.max);
        compute.SetFloat("_DensityZero", _DensityZero);
        compute.SetFloat("_Stiffness", _Stiffness);
        compute.SetFloat("_SmoothingRadius", GridCubeRadius);
        compute.SetFloat("_StiffnessNear", _StiffnessNear);
        compute.SetFloat("_InteractionRadius", _InteractionRadius);
        compute.SetInt("_TotalGridCellCount", (int)(GridCubeRadius * GridCubeRadius * GridCubeRadius));

        foreach (var kernel in Kernels)
        {
            compute.SetBuffer(kernel, Properties, entityPropertiesBuffer);
            compute.SetBuffer(kernel, OldPositions, entityOldPositionBuffer);
        }

        material.SetBuffer(Properties, entityPropertiesBuffer);
    }

    private void GpuDrawEntity(float dt)
    {
        compute.SetVector("_Gravity", _Gravity);
        compute.SetFloat("_DeltaTime", dt);

        foreach (var kernel in Kernels)
        {
            compute.Dispatch(kernel, Mathf.CeilToInt(EntityCount / 64f), 1, 1);
        }

        Graphics.DrawMeshInstancedIndirect(mesh, 0, material, bounds, argsBuffer);

        //_entityProperties = new Particle[EntityCount];
        //entityPropertiesBuffer.GetData(_entityProperties);
    }

    private Mesh CreateQuad(float width = 1f, float height = 1f)
    {
        // Create a quad mesh.
        var mesh = new Mesh();

        float w = width * .5f;
        float h = height * .5f;
        var vertices = new Vector3[4] {
            new Vector3(-w, -h, 0),
            new Vector3(w, -h, 0),
            new Vector3(-w, h, 0),
            new Vector3(w, h, 0)
        };

        var tris = new int[6] {
            // lower left tri.
            0, 2, 1,
            // lower right tri
            2, 3, 1
        };

        var normals = new Vector3[4] {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
        };

        var uv = new Vector2[4] {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(1, 1),
        };

        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.normals = normals;
        mesh.uv = uv;

        return mesh;
    }

    #endregion

    #region Prefab

    private void SetupPrefab()
    {
        Entities = new GameObject[EntityCount];
        StartCoroutine(SpawnEntitiesWithFireRate());
    }

    private void PrefabMove()
    {
        for (int i = 0; i < EntityCount; i++)
        {
            Entities[i].transform.position = Vector3.MoveTowards(Entities[i].transform.position,
                Random.insideUnitSphere * EntityCount * 0.08f,
                moveSpeed * Time.fixedDeltaTime);
        }
    }
    
    IEnumerator SpawnEntitiesWithFireRate()
    {
        for (int i = 0; i < EntityCount; i++)
        {
            //yield return new WaitForSeconds(fireRate);
            var position = Random.insideUnitSphere * EntityCount * 0.008f;
            Entities[i] = Instantiate(EntityPrefab, EntityStart.position + position, Quaternion.identity, transform);
            Entities[i].name = $"{EntityPrefab.name} ({i})";
        }

        yield break;
    }

    #endregion

    private void Start()
    {
        if (GPUDRAW)
        {
            Setup();
        }
        else
        {
            SetupPrefab();
        }
    }

    private void Update()
    {
        if (GPUDRAW)
        {
            GpuDrawEntity(Time.deltaTime);
        }
        else
        {
            PrefabMove();
        }
    }

    private void OnDisable()
    {
        if (entityPropertiesBuffer != null)
        {
            entityPropertiesBuffer.Release();
        }

        entityPropertiesBuffer = null;

        if (entityOldPositionBuffer != null)
        {
            entityOldPositionBuffer.Release();
        }

        entityOldPositionBuffer = null;
        
        if (argsBuffer != null)
        {
            argsBuffer.Release();
        }

        argsBuffer = null;
    }
}
