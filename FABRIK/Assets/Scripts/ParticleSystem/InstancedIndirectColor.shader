Shader "Custom/InstancedIndirectColor" {
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }
    SubShader{
        Tags { "RenderType" = "Opaque" }
    	Cull Off

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex   : POSITION;
                float2 uv       : TEXCOORD0;
                float4 color    : COLOR;
            };

            struct v2f {
                float4 vertex   : SV_POSITION;
                float2 uv       : TEXCOORD0;
                fixed4 color    : COLOR;
            };

            struct Particle {
                float3 position;
                float3 velocity;
                float4 color;
            };

            sampler2D _MainTex;
            StructuredBuffer<Particle> _Properties;

            v2f vert(appdata_t i, uint instanceID: SV_InstanceID) 
            {
                v2f o;/*
                i.vertex.xyz += _Properties[instanceID].position;
                o.vertex = UnityObjectToClipPos(i.vertex);*/
                o.uv = i.uv;
                o.color = _Properties[instanceID].color;

                float3 worldPivot = _Properties[instanceID].position;
                const float4 viewPos = mul(UNITY_MATRIX_V, float4(worldPivot, 1)) + float4(i.vertex.xyz, 0);
                o.vertex = mul(UNITY_MATRIX_P, viewPos);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target {
                float4 col = i.color * tex2D(_MainTex, i.uv);
                clip(col.a - 0.5);
                return col;
            }

            ENDCG
        }
    }
}
