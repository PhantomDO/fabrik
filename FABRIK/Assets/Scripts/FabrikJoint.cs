﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FabrikJoint : MonoBehaviour
    {
        [SerializeField] private float m_SwingConstraint = float.NaN;
        public float SwingConstraint => m_SwingConstraint * .5f * Mathf.Deg2Rad;
        public bool SwingConstrained => !float.IsNaN(m_SwingConstraint);

        [SerializeField] private float m_TwistConstraint = float.NaN;
        public float TwistConstraint => m_TwistConstraint * .5f * Mathf.Deg2Rad;
        public bool TwistConstrained => !float.IsNaN(m_TwistConstraint);

        private FabrikJoint m_Parent = null;
        public FabrikJoint Parent => m_Parent;

        private Vector3 m_Position;
        public Vector3 Position
        {
            get => transform.parent != null ? m_Position : transform.position;
            set => m_Position = value;
        }
        
        private Quaternion m_Rotation;
        public Quaternion Rotation
        {
            get => transform.parent != null ? m_Rotation : transform.rotation;
            set => m_Rotation = value;
        }

        private float m_ParentDistance = 0.0f;
        public float ParentDistance => m_ParentDistance;
        
        public void UpdateTransform()
        {
            Quaternion x90 = new Quaternion(Mathf.Sqrt(0.5f), 0.0f, 0.0f, Mathf.Sqrt(0.5f));
            transform.rotation = Rotation * x90;
            transform.position = Position;
        }

        public void ApplyConstraints(Vector3 direction)
        {
            if (!m_Parent)
            {
                Rotation = Quaternion.LookRotation(direction);
            }
            else
            {
                // No axis are constrained
                if (!SwingConstrained && !TwistConstrained)
                {
                    Rotation = Quaternion.LookRotation(direction, m_Parent.Rotation * Vector3.up);
                }
                else
                {
                    Quaternion global = Quaternion.LookRotation(m_Parent.Rotation * transform.localPosition, m_Parent.Rotation * Vector3.up);
                    Quaternion local = Quaternion.Inverse(global) * Quaternion.LookRotation(direction);
                    Quaternion swing, twist;

                    Decompose(local, Vector3.forward, out swing, out twist);

                    if (SwingConstrained) swing = Constrain(swing, SwingConstraint);
                    if (TwistConstrained) twist = Constrain(twist, TwistConstraint);

                    Rotation = global * swing * twist;
                }
            }
        }
        
        #region QuaternionExtension

        private void Decompose(Quaternion quaternion, Vector3 direction, out Quaternion swing, out Quaternion twist)
        {
            Vector3 vector = new Vector3(quaternion.x, quaternion.y, quaternion.z);
            Vector3 projection = Vector3.Project(vector, direction);

            twist = new Quaternion(projection.x, projection.y, projection.z, quaternion.w).normalized;
            swing = quaternion * Quaternion.Inverse(twist);
        }
        
        private Quaternion Constrain(Quaternion quaternion, float angle)
        {
            float magnitude = Mathf.Sin(0.5F * angle);
            float sqrMagnitude = magnitude * magnitude;

            Vector3 vector = new Vector3(quaternion.x, quaternion.y, quaternion.z);

            if (vector.sqrMagnitude > sqrMagnitude)
            {
                vector = vector.normalized * magnitude;

                quaternion.x = vector.x;
                quaternion.y = vector.y;
                quaternion.z = vector.z;
                quaternion.w = Mathf.Sqrt(1.0F - sqrMagnitude) * Mathf.Sign(quaternion.w);
            }

            return quaternion;
        }

        #endregion

        #region Unity

        private void Awake()
        {
            m_ParentDistance = 0.0f;
            if (transform.parent != null)
            {
                m_ParentDistance = Vector3.Distance(transform.position, transform.parent.position);
                if (!transform.parent.TryGetComponent(out m_Parent))
                {
                    Debug.Log($"No joint parent found");
                }
                
            }

            Rotation = transform.rotation;
            Position = transform.position;
        }

        private void OnDrawGizmos()
        {
            if (TryGetComponent(out MeshFilter meshFilter))
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireCube(meshFilter.sharedMesh.bounds.center, meshFilter.sharedMesh.bounds.size);
            }
        }

        #endregion
    }
}