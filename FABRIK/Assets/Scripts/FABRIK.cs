using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;

public class FABRIK : MonoBehaviour
{
    public Transform ChainTarget;
    public FabrikJoint ChainRoot;
    public FabrikJoint ChainEnd;

    public float Tolerance = 0.01f;
    
    private float m_TotalDistance;
    private FabrikJoint[] m_Joints;
    private float[] m_Distances;

    public bool _hasPlanted;

    void Start()
    {
        FabrikJoint current = ChainEnd;
        List<FabrikJoint> joints = new List<FabrikJoint>();
        List<float> distances = new List<float>();
        while (current != null)
        {
            joints.Insert(0, current);
            distances.Insert(0, current.ParentDistance);
            m_TotalDistance += current.ParentDistance;
            current = current.Parent;
        }
        joints.Insert(0, ChainRoot);

        m_Joints = joints.ToArray();
        m_Distances = distances.ToArray();
    }

    void Update()
    {
        CreateChain();

        for (int i = 0; i < m_Joints.Length - 1; i++)
        {
            var j0 = m_Joints[i].Position;
            var j1 = m_Joints[i + 1].Position;
            Debug.DrawLine(j0, j1, Color.green);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns> the new joints positions </returns>
    public void CreateChain()
    {
        if (m_Joints.Length <= 0) return;

        Vector3 tPos = ChainTarget.position;
        var initP0 = ChainRoot.Position;
        var distEndTarget = Vector3.Distance(tPos, m_Joints[m_Joints.Length - 1].Position);
        
        // backward search
        if (distEndTarget > Tolerance)
        {
            m_Joints[m_Joints.Length - 1].Position = tPos;
            for (int i = m_Joints.Length - 2; i >= 0; i--)
            {
                var direction = (m_Joints[i].Position - m_Joints[i + 1].Position).normalized;
                m_Joints[i].Position = m_Joints[i + 1].Position + (direction * m_Distances[i]);
            }
        }

        // forward search
        m_Joints[0].Position = initP0;
        m_Joints[1].Position = m_Joints[0].Position + m_Joints[0].Rotation * Vector3.forward * m_Distances[0];
        for (int i = 2; i < m_Joints.Length; i++)
        {
            var direction = (m_Joints[i].Position - m_Joints[i - 1].Position).normalized;
            if (i - 1 == 0)
            {
                Debug.Log($"Looking for root constrains");
            }
            m_Joints[i - 1].ApplyConstraints(direction);
            m_Joints[i].Position = m_Joints[i - 1].Position + m_Joints[i - 1].Rotation * Vector3.forward * m_Distances[i - 1];
        }
        
        for (int i = 1; i < m_Joints.Length; i++) m_Joints[i].UpdateTransform();
    }
}
